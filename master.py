from connector import Connector
import logging
import sys
import time
import datetime
from threading import Timer, Thread
from interfaces import Camera, Feeder
from settings import TASK_CHECK_PERIOD
from servertask import ServerTask


def local_debug():
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    connector = Connector()
    task = connector.get_task()
    print(task)

    img_addr = "/home/boris/pi/host/debug/django.jpg"
    #task.task_type = "take_photo"
    task.set_photo(img_addr)
    print(str(task))
    task = connector.finalize_task(task)
    print(task.finished)
    print(task.failed_counts)
    print(task)


class HostMaster:
	connector = Connector()
	camera = Camera()
	feeder = Feeder()

	def main(self):
		while(True):
			self.get_next_task()
			time.sleep(TASK_CHECK_PERIOD.seconds)
            
	def get_next_task(self):
		task = connector.get_tast()
		task = ServerTask(task_type="take_photo", task_id=1)
		if task:
			Thread(target=getattr(self,task.task_type), args=(task,)).start()

	def take_photo(self, task):
		task.set_photo(self.camera.take_photo())
		connector.finalize

	def get_photos(self):
		pass

	def get_level_water(self):
		feeder.getLocalWaterLevel()

	def add_level_water(self):
		feeder.giveWater()

	def get_report(self):
		pass


if __name__ == "__main__":
	logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
	master = HostMaster()
	master.main()


#def local_debug():
#    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
#
#    connector = Connector()
#    task = connector.get_task()
#    img_addr = "/home/boris/pi/host/debug/angular.jpg"
#    #task.task_type = "take_photo"
#    #task.set_photo(img_addr)
#    print(str(task))
#    #task = connector.finalize_task(task)
#    #print(task.finished)
#    #print(task.failed_counts)
#    #print(task)
