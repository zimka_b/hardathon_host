TAKE_PHOTO = 'take_photo'

GET_LEVEL_WATER = 'get_level_water'
ADD_LEVEL_WATER = 'add_level_water'
GET_GLOBAL_LEVEL_WATER = 'get_global_level_water'

GET_LEVEL_FOOD = 'get_level_food'
ADD_LEVEL_FOOD = 'add_level_food'
GET_GLOBAL_LEVEL_FOOD = 'get_global_level_food'
TASK_TYPES = (
    TAKE_PHOTO,
    GET_LEVEL_WATER,
    ADD_LEVEL_WATER,
    GET_GLOBAL_LEVEL_WATER,
    GET_LEVEL_FOOD,
    ADD_LEVEL_FOOD,
    GET_GLOBAL_LEVEL_FOOD,
)

FAILED_MAX = 3


class ServerTask:
    def __init__(self, task_type, task_id, result=""):
        if not task_type in TASK_TYPES:
            raise TypeError("Not allowed task_type")
        self.task_type = task_type
        self.task_id = task_id
        self.result = result
        self.failed_counts = 0

        self.finished = False

        #these fields for photos only
        self.need_post_photo = False #False for non-photo, false for posted photo
        self.photo_addr = None

    @property
    def is_photo(self):
        if self.task_type == TAKE_PHOTO:
            return True
    @property
    def is_failed(self):
        return self.failed_counts >= FAILED_MAX

    def set_photo(self, addr):
        self.photo_addr = addr
        self.need_post_photo = True

    def __str__(self):
        return "id:'{}', type: '{}', result:'{}'".format(self.task_id, self.task_type, self.result)
