from CaptureFunctions import WatcherPC
from multiprocessing import Pipe, Process
from threading import Thread

class CameraServer:
	def __init__(self, pipe):
		self.pipe = pipe
		self.watcher = WatcherPC()
		Thread(target=self.watcher.catch_wrapper).start()
		
	def start(self):
		while True:
			task = self.pipe.recv()
			if task:
				getattr(self,task)()

	def take_photo(self):
		self.pipe.send(self.watcher.take_picture())

	def is_occupied(self):
		self.pipe.send(self.watcher.check_cat())


class Camera:
	def __init__(self):
		self.pipe, pipe = Pipe(True)
		def camera_server_main(pipe):
			CameraServer(pipe).start()
		Process(target = camera_server_main, args=(pipe,)).start()

	def take_photo(self):
		self.pipe.send("take_photo")
		return(self.pipe.recv())

	def is_occupied(self):
		self.pipe.send("is_occupied")
		return(self.pipe.recv())
