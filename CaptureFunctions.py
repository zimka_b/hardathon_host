#!/bin/python2

import time
import datetime
import numpy as np
import cv2
from threading import Lock, Thread
from settings import MEDIA_ROOT

class Watcher(object):
    
    def __init__(self):
        self.square_threshold = 50

    def save_picture(self, frame):
        #directory = pics
        #cv2.imwrite('{0}/picture_data.png'.format(directory), frame)
        filename = '{0}/picture_{1}.png'.format(MEDIA_ROOT, str(datetime.datetime.now()).replace(" ", "_"))
        cv2.imwrite(filename, frame)
        print('Picture saved')
        return filename

    def catch_motion(self, frame1, frame2):
        height, width, channels = frame2.shape
        pixels = frame2.size
        #pixels = width*height

        # Our operations on the frame come here
        #gray = cv2.cvtColor(frame2, cv2.COLOR_BGR2GRAY)
        gray = cv2.cvtColor(cv2.subtract(frame2,frame1), cv2.COLOR_BGR2GRAY)
        # Some filtering may be applied here...
        #
        #...

        maxval = 127
        ret, thresholded = cv2.threshold(gray,32,127,cv2.THRESH_BINARY)
        square = 0
        # Count white pixels
        for x in range(height):
            for y in range(width):
                if thresholded.item(x,y) == maxval:
                    square += 1

        #cv2.imshow('frame', thresholded)

        if square > pixels/self.square_threshold:
            print('Movement!')
            #cv2.imshow('delta', thresholded)
            cv2.imwrite('last_delta.png', thresholded)
            return True
        else:
            return False


class WatcherPC(Watcher):

    def __init__ (self):
        self.cap = cv2.VideoCapture(2)
        self.captureLock = Lock()
        self.cat_found = False
        self.catLock = Lock()
        self.stop = False
        self.stopLock = Lock()
        print('Camera captured')

    def take_picture(self):
        self.captureLock.acquire()
        ret, frame = self.cap.read()
        time.sleep(3)
        self.captureLock.release()
        return self.save_picture(frame)

    def check_cat(self):
        self.catLock.acquire()
        retval = self.cat_found
        self.catLock.release()
        return retval

    def stop_watching (self):
        self.stopLock.acquire()
        self.stop = True
        self.stopLock.release()

    def background_filter(self):
        fgbg = cv2.createBackgroundSubtractorMOG2()
        while(1):
            ret, frame = self.cap.read()

            fgmask = fgbg.apply(frame)

            cv2.imshow('frame',fgmask)
            k = cv2.waitKey(30) & 0xff
            if k == 27:
                break

        self.cap.release()
        cv2.destroyAllWindows()

    def catch_wrapper(self):
        self.timepoint = time.time()
        print('Start overwatching')
        while(True):
            # Capture frame-by-frame
            self.captureLock.acquire()
            ret, frame1 = self.cap.read()
            for x in range(1,9):
                ret, frame = self.cap.read()
                cv2.imshow('frame', frame)

            ret, frame2 = self.cap.read()
            self.captureLock.release()
            #cv2.imshow('frame', frame2)

            motion_found = self.catch_motion(frame1,frame2)
            
            # Newly read motion_found
            if motion_found:
                self.timepoint = time.time()

            if time.time() - self.timepoint > 15:
                motion_found = False

            self.catLock.acquire()
            self.cat_found = motion_found
            self.catLock.release()

            self.stopLock.acquire()
            if self.stop:
                #cv2.imwrite('pics/last_frame.png', gray)
                self.stopLock.release()
                break

            self.stopLock.release()

        # When everything done, release the capture
        #self.cap.release()
        cv2.destroyAllWindows()

class WatcherRasp(Watcher):

    def __init__ (self):
        from picamera import PiCamera
        from picamera.array import PiRGBArray
        super(WatcherRasp, self).__init__()
        # initialize the camera and grab a reference to the raw camera capture
        camera = PiCamera()
        camera.resolution = (640, 480)
        camera.framerate = 32
        self.rawCapture = PiRGBArray(camera, size=(640, 480))
        self.cameraLock = Lock()
        self.cat_found = False
        self.catLock = Lock()
        self.stop = False
        self.stopLock = Lock()
        print('Camera captured')

        # allow the camera to warmup
        time.sleep(0.1)
        self.frame_stream = camera.capture_continuous(self.rawCapture, format="bgr", use_video_port=True)

    def take_picture(self):
        self.cameraLock.acquire()
        frame = self.frame_stream.next().array
        self.rawCapture.truncate(0)
        self.cameraLock.release()
        return self.save_picture(frame)

    def check_cat(self):
        self.catLock.acquire()
        retval = self.cat_found
        self.catLock.release()
        return retval

    def stop_watching (self):
        self.stopLock.acquire()
        self.stop = True
        self.stopLock.release()

    def catch_wrapper(self):
        self.timepoint = time.time()
        print('Start overwatching')
        motion_found = False
        while(True):
            self.cameraLock.acquire()
            frame1 = self.frame_stream.next().array
            self.rawCapture.truncate(0)

            for x in range(1,9):
                frame_stream.next()
                rawCapture.truncate(0)

            frame2 = self.frame_stream.next().array
            self.rawCapture.truncate(0)
            self.cameraLock.release()

            motion_found = self.catch_motion(frame1,frame2)
            
            # Newly read motion_found
            if motion_found:
                self.timepoint = time.time()

            if time.time() - self.timepoint > 15:
                motion_found = False

            self.catLock.acquire()
            self.cat_found = motion_found
            self.catLock.release()

            self.stopLock.acquire()
            if self.stop:
                #cv2.imwrite('pics/last_frame.png', gray)
                self.stopLock.release()
                break
            self.stopLock.release()
