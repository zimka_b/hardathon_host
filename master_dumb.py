from connector import Connector
from driver import Feeder
from settings import DUMB_PHOTO, TASK_CHECK_PERIOD, LOG_FILE, ENABLE_FEEDER
import random
from time import sleep
import logging as log
from datetime import datetime
from CaptureFunctions import WatcherRasp

class DumbCamera:
    def __init__(self):
        random.seed(137)

    def get_photo(self):
        return DUMB_PHOTO

    def motion_catcher(self):
        return random.randint(0,1)


def task_logger(func):
    def wrapped(*args, **kwargs):
        task = func(*args, **kwargs)
        time = str(datetime.now())
        if task.finished:
            log.info(time + "|Finished:{}".format(str(task)))
        else:
            log.error(time + "|Failed:{}".format(str(task)))
        return task
    return wrapped


class HostMasterDumb:
    connector = Connector()
    camera = WatcherRasp()
    feeder = ENABLE_FEEDER and Feeder()

    def __init__(self):
        log.basicConfig(filename=LOG_FILE, level=log.INFO)
        self.task_name_map = {
            "take_photo": self.take_photo,
            'get_level_water': self.get_level_water,
            'add_level_water': self.add_level_water,
            'get_report': self.get_report
        }
        self.task_map_lambda = lambda x: hasattr(self, x) and getattr(self, x)


    def main(self):
        while(1):
            sleep(int(TASK_CHECK_PERIOD.seconds))
            task = self.get_next_task()
            if not task:
                print("NO TASK")
                continue
            else:
                print(task)
            tast_type = task.task_type
            self.task_map_lambda(tast_type)(task)

    def get_next_task(self):
        task = self.connector.get_task()
        if not task:
            return
        return task

    @task_logger
    def take_photo(self, task):
        photo = self.camera.take_picture()
        task.set_photo(photo)
        return self.connector.finalize_task(task)

    @task_logger
    def get_photos(self, task):
        task.result = "DONE"
        return self.connector.finalize_task(task)

    @task_logger
    def get_level_water(self, task):
        data = "Done"
        if ENABLE_FEEDER:
            data = str(self.feeder.getLocalWaterLevel())
        task.result = data
        return self.connector.finalize_task(task)

    @task_logger
    def add_level_water(self, task):
        data = "Done"
        if ENABLE_FEEDER:
            data = str(self.feeder.giveWater())
        task.result = data
        return self.connector.finalize_task(task)

    @task_logger
    def get_level_food(self, task):
        data = "Done"
        if ENABLE_FEEDER:
            data = str(self.feeder.getLocalFoodLevel())
        task.result = data
        return self.connector.finalize_task(task)

    @task_logger
    def add_level_food(self, task):
        data = "Done"
        if ENABLE_FEEDER:
            data = str(self.feeder.giveFood())
        task.result = data
        return self.connector.finalize_task(task)

    @task_logger
    def get_report(self, task):
        task.result = "Not implemented"
        return self.connector.finalize_task(task)

    @task_logger
    def get_global_level_food(self, task):
        data = "Done"
        if ENABLE_FEEDER:
            data = str(self.feeder.getFoodLevel())
        task.result = data
        return self.connector.finalize_task(task)

    @task_logger
    def get_global_level_water(self, task):
        data = "Done"
        if ENABLE_FEEDER:
            data = str(self.feeder.getWaterLevel())
        task.result = data
        return self.connector.finalize_task(task)
if __name__ == "__main__":
    master = HostMasterDumb()
    master.main()
