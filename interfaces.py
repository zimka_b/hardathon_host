from threading import RLock as RLock
from driver import Feeder as _Feeder
from camerawrapper import Camera


class Feeder:
	lock = RLock()
	_i = _Feeder()

	def giveFood(self):
		with self.locl:
			result = self._i.giveFood()
			return result

	def giveWater(self):
		with self.locl:
			return self._i.giveWater()
	
	def getFoodLevel(self):
		with self.locl:
			return self._i.getFoodLevel()

	def getWaterLevel(self):
		with self.locl:
			return self._i.getWaterLevel()

	def getLocalFoodLevel(self):
		with self.locl:
			return self._i.getLocalFoodLevel()

	def getLocalWaterLevel(self):
		with self.locl:
			return self._i.getLocalWaterLevel()
