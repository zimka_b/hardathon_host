import requests as rq
from settings import BASE_URL, HOST_ID
from servertask import ServerTask
import logging

class ApiUrls():
    def __init__(self, base):
        self.base = base

    @property
    def task_url(self):
        return self.base + "api/host"

    @property
    def photo_post_url(self):
        return self.base + "api/photos"


class Connector:
    """
    Provides interface for server task getting and returning
    If can't finish task several times - marks as finished and drops
    """
    def __init__(self):
        self.api_urls = ApiUrls(BASE_URL)
        self.host_id = HOST_ID
        #keep url of photo we posted  but failed to update task

    def get_task(self, task_id=None):
        """Ask server for a new task or task with given task_id"""
        params = {
            "host_id": self.host_id,
            "task_id":task_id
        }
        answer = rq.get(self.api_urls.task_url, params)
        if answer.status_code != 200:
            mes = "No message"
            try:
                mes = answer.json().get("error")
            except Exception as e:
                print(e)
            logging.error("GET_TASK({}):{}".format(answer.status_code, str(mes)))
            return None
        answer = answer.json()
        if not answer["task_type"]:
            return None
        return ServerTask(task_type=answer["task_type"], task_id=answer["task_id"], result=answer.get("result", ""))

    def finalize_task(self, task):
        """Returns result and notifies server"""
        if not isinstance(task, ServerTask):
            raise ValueError("task must be ServerTask")
        if task.is_photo and task.need_post_photo:
            if not self._post_photo(task):
                return task
        if task.is_failed:
            task.finished = True
            return task

        succeed = self._update_task_status(task.task_id, task.result)
        if succeed:
            task.finished = True
        else:
            task.failed_counts += 1
        return task

    def _update_task_status(self, task_id, result):
        params = {
            "host_id": self.host_id,
            "task_id": task_id,
            "result": result
        }
        response = rq.post(self.api_urls.task_url, params)
        if response.status_code == 200:
            return True
        try:
            message = response.json()['error']
        except:
            message = "no message"
        logging.error("POST update: task_id {}, {}".format(task_id, message))
        return False

    def _post_photo(self, task):
        """Posts photo, changes task object and returns if posted sucessfully """
        addr = task.photo_addr
        url = self.api_urls.photo_post_url
        files = {'image': open(addr, 'rb')}
        response = rq.post(url, {"host_id": self.host_id}, files=files)
        if response.status_code == 200:
            task.need_post_photo = False
            task.result = response.json()["url"]
            return True
        task.failed_counts += 1
        return False
