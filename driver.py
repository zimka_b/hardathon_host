from settings import FEEDER_ADDR

class FeederLocal:
    def __init__(self):
        pass;
    def giveFood(self):
        return 'OK'
    def giveWater(self):
        return 'OK'
    def getFoodLevel(self):
        return 3;
    def getWaterLevel(self):
        return 3;
    def getLocalFoodLevel(self):
        return 3;
    def getLocalWaterLevel(self):
        return 3;
    def stop(self):
        pass

import serial
from time import sleep


class Feeder:
    """
    Methods:
       getLevel() returns int number -  level of food or water
       giveFood/Water() returns 'OK'

    There are a problem with calls in a row! Maybe...
    """
    def __init__(self):
        self.ser = serial.Serial(FEEDER_ADDR, 9600)
        sleep(2)
    def giveFood(self):
        self.ser.write('1')
        data = self.ser.readline()
        return data.strip('\r\n')
    def giveWater(self):
        self.ser.write('2')
        data = self.ser.readline()
        return data.strip('\r\n')
    def getFoodLevel(self):
        self.ser.write('3')
        data = self.ser.readline()
        return int(data.strip('\r\n'))
    def getWaterLevel(self):
        self.ser.write('4')
        data = self.ser.readline()
        return int(data.strip('\r\n'))
    def getLocalFoodLevel(self):
        self.ser.write('5')
        data = self.ser.readline()
        return int(data.strip('\r\n'))
    def getLocalWaterLevel(self):
        self.ser.write('6')
        data = self.ser.readline()
        return int(data.strip('\r\n'))
    def stop(self):
        self.ser.close()

#if __name__ == "__main__":
#    print("To test function press: 1 - giveFood, 2 - giveWater, 3 - getFoodLevel, 4 - getWaterLevel, 5 - getLocalFoodLavel, 6 - getLocalWaterLewel, 7 - exit")
#    test = Feeder()
#    while True:
#        inp = str(input())
#        if inp == '1':
#            print("giveFood = " + str(test.giveFood()))
#        elif inp == '2':
#            print("giveWater = " + str(test.giveWater()))
#        elif inp == '3':
#            print("getFoodLevel = " + str(test.getFoodLevel()))
#        elif inp == '4':
#            print("getWaterLevel = " + str(test.getWaterLevel()))
#        elif inp == '5':
#            print("getLocalFoodLevel = " + str(test.getLocalFoodLevel()))
#        elif inp == '6':
#            print("getLocalWaterLevel = " + str(test.getLocalWaterLevel()))
#        elif inp == '7':
#            test.stop()
#            print test.ser.isOpen()
#            break;
