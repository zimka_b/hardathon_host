import os
import datetime

BASE_URL = "http://ec2-52-57-10-129.eu-central-1.compute.amazonaws.com/"
#BASE_URL = "http://0.0.0.0:8000/"

BASE_PATH = os.path.abspath(os.path.dirname(__file__))

MEDIA_ROOT = BASE_PATH + "/debug"
print(MEDIA_ROOT)
HOST_ID = 1
DUMB_PHOTO = MEDIA_ROOT+'/django.jpg'
TASK_CHECK_PERIOD = 5
LOG_FILE = "log.txt"
ENABLE_FEEDER = True

#FEEDER_ADDR = "COM8"
FEEDER_ADDR = "/dev/ttyACM0"
TASK_CHECK_PERIOD = datetime.timedelta(seconds=2)
